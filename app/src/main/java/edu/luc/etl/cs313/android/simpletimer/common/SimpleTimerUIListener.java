package edu.luc.etl.cs313.android.simpletimer.common;

/**
 * A listener for simple timer events coming from the UI.
 *
 * @author laufer, predey
 */
public interface SimpleTimerUIListener {
    void onClick();
}
