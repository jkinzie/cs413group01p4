package edu.luc.etl.cs313.android.simpletimer.test.model.time;

import static edu.luc.etl.cs313.android.simpletimer.common.Constants.SEC_PER_TICK;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author laufer, predey
 * @see http://xunitpatterns.com/Testcase%20Superclass.html
 */
//TODO PARTIALLY COMPLETED update tests to reflect simple timer functionality and variable names
public abstract class AbstractTimeModelTest {

    private TimeModel model;
    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final TimeModel model) {
        this.model = model;
    }

    /**
     * Verifies that default time remaining is equal to zero
     * This is what the remaining time should be in the initial state
     */
    @Test
    public void testPreconditions() {
        assertEquals(0, model.getRemainingTime());
    }

    /**
     * Verifies that remaining time is incremented correctly.
     */
    @Test
    public void testIncrementRemainingTimeOne() {
        final int rt = model.getRemainingTime();
        model.incRemainingTime();
        assertEquals(rt + SEC_PER_TICK, model.getRemainingTime());
    }

    /**
     * Verifies that remaining time decrements correctly; will not pass if above test has not passed.
     */
    @Test
    public void testDecrementRemainingTimeOne() {
        final int rt = model.getRemainingTime();
        model.incRemainingTime();
        model.incRemainingTime();
        model.decRemainingTime();
        assertEquals(rt, model.getRemainingTime()); //Should be equal to 1 second
    }

}
