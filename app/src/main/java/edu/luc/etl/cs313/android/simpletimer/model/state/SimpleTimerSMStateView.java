package edu.luc.etl.cs313.android.simpletimer.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer, predey
 */
interface SimpleTimerSMStateView {
//TODO as you add functionality, make sure to update the methods in this interface
    // transitions
    void toRunningState();
    void toInitialState();
    void toCountingState();
    void toStartBeepState();

    // actions
    //TODO add actions as they arise, e.g. a beep action.
    void actionInit();
    void actionReset();
    void actionInc();
    void actionDec();
    void actionStart();
    void actionStop();
    void actionUpdateView();
    int actionCheckTime();
    void actionSetClickTime();
    int actionCheckClickTime();
    void actionBeep();
    void actionStopBeep();
    void actionAlarm();
    void actionStopAlarm();

    // state-dependent UI updates
    void updateUIRemainingTime();
}
