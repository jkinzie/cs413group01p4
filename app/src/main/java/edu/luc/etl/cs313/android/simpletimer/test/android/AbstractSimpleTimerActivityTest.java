package edu.luc.etl.cs313.android.simpletimer.test.android;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import android.content.pm.ActivityInfo;
import org.junit.Test;

import android.widget.Button;
import android.widget.TextView;
import edu.luc.etl.cs313.android.simpletimer.R;
import edu.luc.etl.cs313.android.simpletimer.android.SimpleTimerAdapter;
//
//
///**
// * Abstract GUI-level test superclass of several essential stopwatch scenarios.
// *
// * @author laufer
// *
// * TODO move this and the other tests to src/test once Android Studio supports
// * non-instrumentation unit tests properly.
// */
//
////TODO update the tests in this class to reflect the simple timer functionality where appropriate

public abstract class AbstractSimpleTimerActivityTest {

    /**
     * Verifies that the activity under test can be launched.
     */


    public void testActivityCheckTestCaseSetUpProperly() {
        assertNotNull("Activity should be launched successfully", getActivity());
    }

    /**
     * Verifies the following scenario: time is 0.
     *
     * @throws Throwable
     */
    @Test
    public void testActivityScenarioInit() throws Throwable {
        getActivity().runOnUiThread(() -> assertEquals(0, getDisplayedValue()));
    }

    /**
     * testActivityScenarioInc performs these checks:
     * There should be a button that is able to increase the number on the display,
     * and it should be increased the value by 1.
     *
     * @throws Throwable
     */
    @Test
    public void testActivityScenarioRun() throws Throwable {
        getActivity().runOnUiThread(() -> {
            assertEquals(0, getDisplayedValue());
            assertTrue(getButton().performClick());
        });
        runUiThreadTasks();
        getActivity().runOnUiThread(() -> {
            assertEquals(1, getDisplayedValue());
        });
    }


// auxiliary methods for easy access to UI widgets

    protected abstract SimpleTimerAdapter getActivity();

    protected int tvToInt(final TextView t) {
        return Integer.parseInt(t.getText().toString().trim());
    }

    protected int getDisplayedValue() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.seconds);
        return tvToInt(ts);
    }

    protected Button getButton() {
        return (Button) getActivity().findViewById(R.id.click);
    }

    protected String getState() {
        final TextView ts = (TextView) getActivity().findViewById(R.id.stateName);
        return ts.getText().toString();
    }

    protected String getButtonText() {
        final TextView bt = (TextView) getActivity().findViewById(R.id.click);
        return bt.getText().toString();
    }

    /**
     * Explicitly runs tasks scheduled to run on the UI thread in case this is required
     * by the testing framework, e.g., Robolectric.
     */
    protected void runUiThreadTasks() {
        // Robolectric requires us to run the scheduled tasks explicitly!
       // Robolectric.runUiThreadTasks();
    }
}
//public abstract class AbstractSimpleTimerActivityTest {
//
//    /**
//     * Verifies that the activity under test can be launched.
//     */
//    @Test
//    public void testActivityCheckTestCaseSetUpProperly() {
//        assertNotNull("activity should be launched successfully", getActivity());
//    }
//
//    /**
//     * Verifies the following scenario: time is 0.
//     *
//     * @throws Throwable
//     */
//    @Test
//    public void testActivityScenarioInit() throws Throwable {
//        getActivity().runOnUiThread(() -> assertEquals(0, getDisplayedValue())); //runOnUiThread(...) comes from android
//    }
//
//    /**
//     * Verifies the following scenario: time is 0, press start, wait 5+ seconds, expect time 5.
//     *
//     * @throws Throwable
//     */
//    @Test
//    public void testActivityScenarioRun() throws Throwable { //Checks to make sure the stopwatch is displaying "5"
//        getActivity().runOnUiThread(() -> {
//            assertEquals(0, getDisplayedValue());
//            assertTrue(getStartStopButton().performClick());
//        });
//        Thread.sleep(5500); // <-- do not run this in the UI thread!
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> {
//            assertEquals(5, getDisplayedValue()); //getDisplayedValue() is defined in this class.
//            assertTrue(getStartStopButton().performClick());
//        });
//    }
//
//    /**
//     * Verifies the following scenario: time is 0, press start, wait 5+ seconds,
//     * expect time 5, press lap, wait 4 seconds, expect time 5, press start,
//     * expect time 5, press lap, expect time 9, press lap, expect time 0.
//     *
//     * @throws Throwable
//     */
//    @Test
//    public void testActivityScenarioRunLapReset() throws Throwable { //Checks how star/stop and lap/reset buttons work together.
//        getActivity().runOnUiThread(() -> {
//            assertEquals(0, getDisplayedValue());
//            assertTrue(getStartStopButton().performClick());
//        });
//        Thread.sleep(5500); // <-- do not run this in the UI thread!
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> {
//            assertEquals(5, getDisplayedValue());
//            assertTrue(getResetLapButton().performClick());
//        });
//        Thread.sleep(4000); // <-- do not run this in the UI thread!
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> {
//            assertEquals(5, getDisplayedValue());
//            assertTrue(getStartStopButton().performClick());
//        });
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> {
//            assertEquals(5, getDisplayedValue());
//            assertTrue(getResetLapButton().performClick());
//        });
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> {
//            assertEquals(9, getDisplayedValue());
//            assertTrue(getResetLapButton().performClick());
//        });
//        runUiThreadTasks();
//        getActivity().runOnUiThread(() -> assertEquals(0, getDisplayedValue()));
//    }
//
//    // auxiliary methods for easy access to UI widgets
//
//    protected abstract SimpleTimerAdapter getActivity();
//
//    protected int tvToInt(final TextView t) {
//        return Integer.parseInt(t.getText().toString().trim());
//    }
//
//    protected int getDisplayedValue() {
//        final TextView ts = (TextView) getActivity().findViewById(R.id.seconds);
//        return tvToInt(ts);
//    }
//
//    protected Button getStartStopButton() {
//        return (Button) getActivity().findViewById(R.id.click);
//    }
//
//    protected Button getResetLapButton() {
//        return (Button) getActivity().findViewById(R.id.resetLap);
//    }
//
//    /**
//     * Explicitly runs tasks scheduled to run on the UI thread in case this is required
//     * by the testing framework, e.g., Robolectric.
//     */
//    protected void runUiThreadTasks() { }
//}
