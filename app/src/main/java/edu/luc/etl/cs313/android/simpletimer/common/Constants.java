package edu.luc.etl.cs313.android.simpletimer.common;

/**
 * Constants for the time calculations used by the simpletimer.
 */
public final class Constants {

    public static int SEC_PER_TICK = 1;

    private Constants() { }
}