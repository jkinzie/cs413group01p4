package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;


class RunningState implements SimpleTimerState {

    public RunningState(final SimpleTimerSMStateView sm) {
        this.sm = sm;
    }

    private final SimpleTimerSMStateView sm;

    @Override
    public void onClick() {
        //TODO verify this method with tests and requirements
        //A click while the timer is running acts as a 'cancel', reverting the timer to its initial state
        sm.actionStop();
        sm.actionStopBeep();
        sm.actionReset();
        sm.toInitialState();
    }

    @Override
    public void onTick() {
        //A tick while the timer is running updates the
        sm.actionDec();
        sm.toRunningState();
        int time = sm.actionCheckTime();
        if(time==0){
            sm.actionStop();
            sm.toStartBeepState();
            sm.actionAlarm();

        }
    }

    @Override
    public void updateView() {
        sm.updateUIRemainingTime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
