package edu.luc.etl.cs313.android.simpletimer.model;

import edu.luc.etl.cs313.android.simpletimer.common.SimpleTimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpletimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpletimer.model.clock.DefaultClockModel;
import edu.luc.etl.cs313.android.simpletimer.model.state.DefaultSimpleTimerStateMachine;
import edu.luc.etl.cs313.android.simpletimer.model.state.SimpleTimerStateMachine;
import edu.luc.etl.cs313.android.simpletimer.model.time.DefaultTimeModel;
import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * An implementation of the model facade.
 *
 * @author laufer, predey
 */
public class ConcreteSimpleTimerModelFacade implements SimpleTimerModelFacade {

    private SimpleTimerStateMachine stateMachine;

    private ClockModel clockModel;

    private TimeModel timeModel;

    public ConcreteSimpleTimerModelFacade() {
        timeModel = new DefaultTimeModel();
        clockModel = new DefaultClockModel();
        stateMachine = new DefaultSimpleTimerStateMachine(timeModel, clockModel);
        clockModel.setOnTickListener(stateMachine);
    }

    @Override
    public void onStart() {
        stateMachine.actionInit();
    }

    @Override
    public void setUIUpdateListener(final SimpleTimerUIUpdateListener listener) {
        stateMachine.setUIUpdateListener(listener);
    }

    @Override
    public void onClick() {
        stateMachine.onClick();
    }



}
