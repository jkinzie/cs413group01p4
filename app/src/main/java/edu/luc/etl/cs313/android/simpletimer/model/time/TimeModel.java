package edu.luc.etl.cs313.android.simpletimer.model.time;

/**
 * The passive data model of the simple timer.
 * It does not emit any events.
 *
 * @author laufer, predey
 */
public interface TimeModel {
    void resetRemainingTime();
    void incRemainingTime();
    void decRemainingTime();
    int getRemainingTime();
}
