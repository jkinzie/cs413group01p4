package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

class InitialState implements SimpleTimerState {

    public InitialState(final SimpleTimerSMStateView sm) {
        this.sm = sm;
    }

    private final SimpleTimerSMStateView sm;

    @Override
    public void onClick() {
        //TODO Fill in method for stopped state onClick
        // The first click here acts like a counting state action (incrementing the timer)
        // and also changes the state to the counting state (which requires a clock start)
        sm.actionStart();
        sm.actionSetClickTime();
        sm.actionInc();
        sm.toCountingState();

    }

    // Since the clock is stopped in the initial state, the listener shouldn't hear any ticks
    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIRemainingTime();
    }

    // TODO DONE
    @Override
    public int getId() {
        return R.string.INITIAL;
    }
}
