package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

/**
 * @author predey
 */
public class CountingState implements SimpleTimerState {

    public CountingState(final SimpleTimerSMStateView sm) {
        this.sm = sm;
    }

    private final SimpleTimerSMStateView sm;
    int timeCount;

    //TODO DONE Add functionality for when the last click sets the time equal to 99 (the highest possible timer value)
    @Override
    public void onClick() {
        //TODO verify this method with tests and requirements
        timeCount = 0;
        int time = sm.actionCheckTime(); //Checks what is currently displayed on the timer
        if (time >= 99) {
            sm.actionStop();
            sm.toStartBeepState();
            sm.actionBeep();
            sm.actionStart();
            sm.toRunningState();
        } //Handles max time case
        else if(time > 0 && time < 99){
            sm.actionInc();
            sm.actionSetClickTime(); //Every click records the time at which the click occurred
            sm.toCountingState();
        }
        else sm.toInitialState();

    }

    @Override
    public void onTick() {
        // Checks to make see if the user has clicked within the last three second

        timeCount++;
        if (timeCount == 3){
            sm.actionStop();
            sm.toStartBeepState();
            sm.actionBeep();
            sm.actionStart();
            sm.toRunningState();
        }
        // If less than three seconds has elapsed, then we stay in the counting state.
        else sm.toCountingState();
    }
    @Override
    public void updateView() {
        sm.updateUIRemainingTime();
    }

    @Override
    //TODO DONE
    public int getId() {
        return R.string.COUNTING;
    }
}
