package edu.luc.etl.cs313.android.simpletimer.model.state;

import android.media.MediaPlayer;

import java.util.Timer;
import java.util.TimerTask;

import edu.luc.etl.cs313.android.simpletimer.common.SimpleTimerUIUpdateListener;
import edu.luc.etl.cs313.android.simpletimer.model.clock.ClockModel;
import edu.luc.etl.cs313.android.simpletimer.model.time.TimeModel;

/**
 * An implementation of the state machine for the simple timer.
 *
 * @author laufer, predey, kinzie
 */
public class DefaultSimpleTimerStateMachine implements SimpleTimerStateMachine {

    public DefaultSimpleTimerStateMachine(final TimeModel timeModel, final ClockModel clockModel) {
        this.timeModel = timeModel;
        this.clockModel = clockModel;
    }

    private final TimeModel timeModel;
    private final ClockModel clockModel;

    // This is where we store the time since the last click in the Counting State
    // Stored here so it doesn't get wiped out transitioning between ticks in adjacent counting states.
    private int clickTime;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private SimpleTimerState state;
    private MediaPlayer mediaPlayer;

    protected void setState(final SimpleTimerState state) {
        this.state = state;
        uiUpdateListener.updateState(state.getId());
    }

    private SimpleTimerUIUpdateListener uiUpdateListener;

    @Override
    public void setUIUpdateListener(final SimpleTimerUIUpdateListener uiUpdateListener) {
        this.uiUpdateListener = uiUpdateListener;
    }

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onClick()  { state.onClick(); }


    @Override public synchronized void onTick()   { state.onTick(); }

    //we no longer care about run time, only time remaining.
    @Override public void updateUIRemainingTime() { uiUpdateListener.updateTime(timeModel.getRemainingTime()); }
    @Override public void actionBeep()        {
        mediaPlayer = uiUpdateListener.playDefaultALARM();
    }
    @Override public void actionStopBeep()
    { mediaPlayer.stop(); }
    // known states
    private final SimpleTimerState INITIAL_STATE    = new InitialState(this);
    private final SimpleTimerState COUNTING_STATE   = new CountingState(this);
    private final SimpleTimerState START_BEEP_STATE = new StartBeepState(this);
    private final SimpleTimerState RUNNING_STATE    = new RunningState(this);

    // transitions
    @Override public void toInitialState()    { setState(INITIAL_STATE); }
    @Override public void toCountingState()   { setState(COUNTING_STATE); }
    @Override public void toStartBeepState()  { setState(START_BEEP_STATE); }
    @Override public void toRunningState()    { setState(RUNNING_STATE); }

    // actions
    @Override public void actionInit()           { toInitialState(); actionReset(); }
    /**
     * plays one sound when meets condition
     */
    @Override public void actionAlarm()      { startAlarm();}
    /**
     * Cancel the alarmtimer and set the runtime to 00
     */
    @Override public void actionStopAlarm()   {stopAlarm();actionReset();}

    @Override public void actionReset()          { timeModel.resetRemainingTime(); actionUpdateView(); }
    @Override public void actionStart()          { clockModel.start(); }
    @Override public void actionStop()           { clockModel.stop(); }
    @Override public void actionInc()            { timeModel.incRemainingTime(); actionUpdateView(); }

    @Override public void actionDec()            { timeModel.decRemainingTime(); actionUpdateView(); }
    //actionCheckTime() actually returns an int, android studio just thinks there's an error
    @Override public int  actionCheckTime()      { return timeModel.getRemainingTime(); }
    @Override public void actionSetClickTime()   { clickTime = actionCheckTime(); }
    @Override public int  actionCheckClickTime() { return clickTime; }
    @Override public void actionUpdateView()     { state.updateView(); }
    public void onAlarm()       { actionBeep(); }
    private Timer alarmtimer;

    /**
     * Use a timer to play a continually and infinitely arlarm sound when meets a specific condition
     */
    public void startAlarm() {
        alarmtimer = new Timer();
        //added on 4/9/2016
        // The clock model runs onAlarm every 2000 milliseconds
        alarmtimer.schedule(new TimerTask() {
            @Override
            public void run() {
                // fire event
                onAlarm();
            }
        }, /*initial delay*/ 0, /*periodic delay*/ 2000);
    }

    /**
     * Cancel the alarm timer
     */
    public void stopAlarm() {
        alarmtimer.cancel();
    }

}
