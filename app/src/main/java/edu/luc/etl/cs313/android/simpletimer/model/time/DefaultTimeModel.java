package edu.luc.etl.cs313.android.simpletimer.model.time;

import static edu.luc.etl.cs313.android.simpletimer.common.Constants.*;

/**
 * An implementation of the simple timer data model.
 */

//TODO update the getter methods and action methods to reflect the functionality of the simple timer
public class DefaultTimeModel implements TimeModel {

    private int remainingTime = 0;

    @Override
    public void resetRemainingTime() {
        remainingTime = 0;
    }

    @Override
    public void incRemainingTime() {
        remainingTime = remainingTime + SEC_PER_TICK;
    }

    @Override
    public void decRemainingTime() {
        remainingTime = remainingTime - SEC_PER_TICK;
    }

    @Override
    public int getRemainingTime() {
        return remainingTime;
    }

}