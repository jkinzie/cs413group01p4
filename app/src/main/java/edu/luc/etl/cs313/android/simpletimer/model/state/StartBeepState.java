package edu.luc.etl.cs313.android.simpletimer.model.state;

import edu.luc.etl.cs313.android.simpletimer.R;

/**
 * @author predey
 */
public class StartBeepState implements SimpleTimerState {
    //TODO Implement this state based on the other complete/semi-complete states in this directory

    //TODO Figure out how to actually make the app beep!!!
    public StartBeepState(final SimpleTimerSMStateView sm) {
        this.sm = sm;
    }

    private final SimpleTimerSMStateView sm;

    //The two different beeping states are covered using conditional branches in onClick() and onTick()
    //The two different beeping states are covered using conditional branches in onClick() and onTick()
    @Override
    public void onClick() {
        if (sm.actionCheckTime() > 0) {
            //A click doesn't do anything during the beep before the timer starts counting down
            throw new UnsupportedOperationException("onClick");
        }
        else{
            // A click when the time has run out stops the clock and sends the app back to the initial state
            sm.actionStop();
            sm.actionStopAlarm();
            sm.toInitialState();

        }
    }

    @Override
    public void onTick() {
        // Changes states according to its current function
        if (sm.actionCheckTime() > 0) sm.toRunningState();
        else { sm.toStartBeepState();
            sm.actionAlarm();
        }
    }
    @Override
    public void updateView() {
        sm.updateUIRemainingTime();
    }

    @Override
    public int getId() {
        return R.string.STARTBEEPING;
    }


}
