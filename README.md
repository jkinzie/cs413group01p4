# Project 4
# Due Date: Tuesday April 12

# Pair Project 

# Objectives 

Familiarity with
event-driven program execution
state dependence in application (the State design pattern)
Model-View-Adapter architecture
Android framework

# Description 

In this project, you will implement a very simple timer as an Android application. There is no explicit Project link for this project, but you should start from this example stopwatch Android Studio project: https://github.com/LoyolaChicagoCode/stopwatch-android-java. 

You may also find parts of this clickcounter Android Studio project helpful for managing the Activity life cycle, especially saving and restoring state (eg, when the device is rotated) and playing a sound as part of the Activity (the "beep" needed in Project 4): https://github.com/LoyolaChicagoCode/clickcounter-android-java. 
## Functional Requirements (55%) 

The timer has the following controls:
(0.5) One two-digit display of the form 88.
(0.5) One multi-function button.
The timer behaves as follows:
(0.5) The timer always displays the remaining time in seconds.
(0.5) Initially, the timer is stopped and the (remaining) time is zero.
(0.5) If the button is pressed when the timer is stopped, the time is incremented by one up to a preset maximum of 99. (The button acts as an increment button.)
(0.5) If the time is greater than zero and three seconds elapse from the most recent time the button was pressed, then the timer beeps once and starts running.
Note: if the time reaches the preset maximum of 99 the timer acts the same way as if three seconds had elapsed - it beeps and starts running.
(0.5) While running, the timer subtracts one from the time for every second that elapses.
In particular, the display only changes 1 second after the 3-second timeout occurs or the timer value reaches 99.
(0.5) If the timer is running and the button is pressed, the timer stops and the time is reset to zero. (The button acts as a cancel button.)
(0.5) If the timer is running and the time reaches zero by itself (without the button being pressed), then the timer stops and the alarm starts beeping continually and indefinitely.
(0.5) If the alarm is sounding and the button is pressed, the alarm stops sounding; the timer is now stopped and the (remaining) time is zero. (The button acts as a stop button.)
(0.5) The timer handles rotation by continuing in its current state.
Extra Credit (worth 20%)
(2.0 extra credit) Add a 2-digit editable text area, editable only when the timer is stopped, where the user can type in the time and then press enter or click the button to start the timer.
Note that this will require the emulator or device to have a physical or on-screen keyboard to enter the digits, and it is a non-trivial addition to the project!!
## Nonfunctional Requirements (25%)

Ensure your application is testable.
(1.5) Ensure your application includes comprehensive unit, integration, and functional tests using the techniques from the clickcounter and stopwatch examples where appropriate. (See also http://developer.android.com/tools/testing/testing_android.html)
(0.5) Follow the design principles discussed so far.
Maintain a clear, responsibility-based separation among the different building blocks. It is recommended that you start with the stopwatch example.
The adapter should be lean (as in the stopwatch example).
Most of the complexity should be buried in the model (also as in the stopwatch example).
Unlike the stopwatch example, the dynamic model (state machine) has guards involving the current time. Think carefully how to inject the dependency on the time model into the state machine. Do not use static members!
(0.5 extra credit) Use of the state pattern (APPP chapter 36) is recommended. If you reuse and update the stopwatch example, you will likely get this extra credit.
Generally follow good Android development practice: http://developer.android.com
Use this Timer class: http://developer.android.com/reference/java/util/Timer.html
(0.5) For beeping, use media playback to play a notification sound (or some other suitable mechanism):
http://stackoverflow.com/questions/10335057/play-notification-default-sound-only-android
use getApplicationContext() to obtain the required context reference
note that clickcounter plays a sound, so you can review how it does that
## Written Part/Documentation (20%) 

Please include these deliverables in the doc folder of your project 4.
(0.5) Include the model from a future in-class group activity. (Minimally, a cell phone scan of your drawing is acceptable.)
(0.25) Use inline comments to document design details in the code.
(0.25) Use javadoc comments to document how to use the abstractions (interfaces, classes) you designed.
Include a brief (300-500 words) report on
(0.5) Your pair development journey during this project. Focus on aspects you find noteworthy, e.g., process, pairing, testing, design decisions, refactoring, use of the repository.
(0.5) The relationship between your state machine model from this project and your actual code. Possible talking points are:
What are the similarities and differences between model and code?
Is it more effective to code or model first?
Now that you have the code, what, if any, changes would you make to your model? 
# Grading Criteria

Stated percentages for the major categories
Stated points for the specific items
Total 10 points with opportunities for up to 2.5 points of extra credit
Deductions of up to 1 point for
deviation from the required project folder structure (see clickcounter)
inability to run and/or test
# How to submit 

As the first step in working on this project, one of your larger team members will push the code skeleton to a private Bitbucket repository shared among all of you and your instructor and TA. The name of the repository is cs413f15groupNp4, where N is your group number found in the UML class exercise slides from Week 6. When your work is ready to be graded, please notify your instructor and TA via Piazza and/or submitting a note in the Sakai Project 4 assignment.