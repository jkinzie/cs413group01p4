package edu.luc.etl.cs313.android.simpletimer.model;

import edu.luc.etl.cs313.android.simpletimer.common.SimpleTimerUIUpdateSource;
import edu.luc.etl.cs313.android.simpletimer.common.SimpleTimerUIListener;


/**
 * A thin model facade. Following the Facade pattern,
 * this isolates the complexity of the model from its clients (usually the adapter).
 *
 * @author laufer
 */
public interface SimpleTimerModelFacade extends SimpleTimerUIListener, SimpleTimerUIUpdateSource {
    void onStart();
}
